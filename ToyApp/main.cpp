#include <iostream>
#include <vector>

#include "People.h"

int main(int argc, char** argv)
{
  std::cout << "hi mom!" << std::endl;

  // make a container of all our peeps
  std::vector<Person*> myPeeps;

  // add peeps to the vector
  myPeeps.push_back(new Person());
  myPeeps.push_back(new Charlie());
  myPeeps.push_back(new Devin());
  myPeeps.push_back(new Joe());
  myPeeps.push_back(new George());
  myPeeps.push_back(new George2());
  myPeeps.push_back(new George3());
  myPeeps.push_back(new George4());
  myPeeps.push_back(new George5());
  myPeeps.push_back(new David());
  myPeeps.push_back(new Cameron());
  myPeeps.push_back(new Manny());
  myPeeps.push_back(new Chen());
  myPeeps.push_back(new YenTing());
  myPeeps.push_back(new Tianchen());
  myPeeps.push_back(new Kamil());

  // say hello
  for (auto person : myPeeps)
  {
    std::cout << "Hi " << person->getName() << "!" << std::endl;
  }

  getchar();
  return 0;
}